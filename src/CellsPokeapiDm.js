import { LitElement, html, } from 'lit-element';

import '@bbva-web-components/bbva-core-generic-dp/bbva-core-generic-dp.js';
/**
@customElement cells-pokeapi-dm
*/
export class CellsPokeapiDm extends LitElement {
  static get is() {
    return 'cells-pokeapi-dm';
  }

  // Declare properties
  static get properties() {
    return {
      host: { type: String }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.host = "https://pokeapi.co/api/v2/";
  }

  // Define a template
  render() {
    return html`
      <bbva-core-generic-dp
        id="pokemonListDP"
        host="${this.host}"
        method="GET"
        @request-success="${this._listSuccess}"
        @request-error="${this._listError}">
      </bbva-core-generic-dp>
      <bbva-core-generic-dp
        id="pokemonDetailDP"
        method="GET"
        @request-success="${this._detailSuccess}"
        @request-error="${this._detailError}">
      </bbva-core-generic-dp>
    `;
  }

  getPokemonList(pageKey, pageSize) {
    let dp = this.shadowRoot.getElementById('pokemonListDP');
    dp.setAttribute('path', "pokemon?limit=" + pageSize + "&offset=" + pageKey*pageSize);
    dp.generateRequest();
  }

  getPokemonDetail(url) {
    let dp = this.shadowRoot.getElementById('pokemonDetailDP');
    dp.setAttribute('host', url);
    dp.generateRequest();
  }

  _listSuccess(ev) {
    this.dispatchEvent(new CustomEvent('list-success', {
      bubbles: true,
      composed: true,
      detail: ev.detail,
    }));
  }

  _listError(ev) {
    this.dispatchEvent(new CustomEvent('list-error', {
      bubbles: true,
      composed: true,
      detail: ev.detail,
    }));
  }

  _detailSuccess(ev) {
    this.dispatchEvent(new CustomEvent('detail-success', {
      bubbles: true,
      composed: true,
      detail: ev.detail,
    }));
  }

  _detailError(ev) {
    this.dispatchEvent(new CustomEvent('detail-error', {
      bubbles: true,
      composed: true,
      detail: ev.detail,
    }));
  }

}
